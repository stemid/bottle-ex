
import json
from datetime import datetime
from pprint import pprint as pp
from uuid import UUID

from redis import Redis
from rq import Queue

from bottle import Bottle
from bottle import request, response, template, static_file

from .logger import AppLogger


class BottleExample(Bottle):

    def __init__(self, name, **kw):
        super(BottleExample, self).__init__()
        self.name = name

        #app.router.add_filter('uuid', uuid_filter)
        self.config = kw.get('config')

        logger = AppLogger(self.name, self.config).logger

        self.r = Redis(
            host=self.config.get('app', 'redis_host'),
            port=self.config.getint('app', 'redis_port')
        )

        logger.info('Redis initialized')

        # Setup routes
        self.route('/static/<path:path>', callback=self.serve_static)
        self.route('/', callback=self.index)
        self.route(
            '/api/v1/client/add',
            callback=self.add_client,
            method='POST'
        )


    ## Custom UUID route filter for bottle.py
    @staticmethod
    def uuid_filter(config):
        # Should catch UUIDv4 type strings
        regexp = r'[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}'

        def to_python(match):
            return UUID(match, version=4)

        def to_url(uuid):
            return str(uuid)

        return regexp, to_python, to_url


    # Serve static files
    def serve_static(self, path):
        return static_file(path, root=self.config.get('app', 'static_dir'))


    def index(self):
        return template('index')


    def add_client(self):
        response.set_header('Content-type', 'application/json')

        # Import Wsgi environ
        environ = {}
        for key in request.environ:
            value = request.environ.get(key)
            if isinstance(value, (int, str, float, dict, set, tuple)):
                environ[key] = value

        # Get client IP
        client_ip = environ.get(
            'HTTP_X_FORWARDED_FOR',
            environ.get('REMOTE_ADDR')
        )

        client_count = self.r.get(client_ip)

        if not client_count:
            client_count = 1
        else:
            client_count = int(client_count)
            client_count += 1

        self.r.set(client_ip, client_count)

        return {
            'status': 'OK',
            'client_ip': client_ip,
            'client_count': client_count
        }

