import sys

from logging import StreamHandler
from logging import Formatter, getLogger, getLevelName, DEBUG, WARN, INFO
from logging.handlers import SysLogHandler, RotatingFileHandler

class AppLogger(object):

    def __init__(self, name, config):
        """This initializes with a RawConfigParser object to get most of its
        default settings. 
        """

        formatter = Formatter(config.get('logging', 'log_format'))
        self.logger = getLogger(name)

        if config.get('logging', 'log_handler') == 'syslog':
            syslog_address = config.get('logging', 'syslog_address')
            default_facility = SysLogHandler.LOG_LOCAL0

            if syslog_address.startswith('/'):
                h = SysLogHandler(
                    address=syslog_address,
                    facility=default_facility
                )
            else:
                h = SysLogHandler(
                    address=(
                        config.get('logging', 'syslog_address'),
                        config.get('logging', 'syslog_port')
                    ),
                    facility=default_facility
                )
        elif config.get('logging', 'log_handler') == 'file':
            h = RotatingFileHandler(
                config.get('logging', 'log_file'),
                maxBytes=config.getint('logging', 'log_max_bytes'),
                backupCount=config.getint('logging', 'log_max_copies')
            )
        else:
            h = StreamHandler(stream=sys.stdout)

        h.setFormatter(formatter)
        self.logger.addHandler(h)

        try:
            log_level = getLevelName(
                config.get('logging', 'log_level').toupper()
            )
        except:
            log_level = DEBUG
            pass

        self.logger.setLevel(log_level)

