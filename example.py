from argparse import ArgumentParser, FileType
from configparser import RawConfigParser

from app import BottleExample

DEFAULT_CONFIG = './app.cfg'

parser = ArgumentParser()
parser.add_argument(
    '-v', '--verbose',
    action='count',
    default=False,
    dest='verbose',
    help='Verbose output'
)

parser.add_argument(
    '-c', '--config',
    default='./app.local.cfg',
    help='One additional configuration file path to override defaults'
)

config = RawConfigParser()
config.readfp(open(DEFAULT_CONFIG))

if __name__ == '__main__':
    args = parser.parse_args()

    config.read([args.config])

    app = BottleExample('bottle-ex', config=config)

    app.run(
        host=config.get('app', 'listen_host'),
        port=config.getint('app', 'listen_port')
    )
else:
    app = BottleExample('bottle-ex', config=config)
    application = app
